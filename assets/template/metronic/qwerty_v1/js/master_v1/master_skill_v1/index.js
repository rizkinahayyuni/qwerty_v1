$(document).ready(function () {
    const initiateDatatables = () => {
        table = $("#table_master_skill").DataTable({
            "lengthMenu": [10, 50, 100, 200],
            "pageLength": "50",
            "autoWidth": true,
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "ajax": {
                "url": `${target}/json`,
                "type": 'POST',
            },
        });
    }

    $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        let conf = confirm('Apakah anda yakin akan menghapus data ini ?');
        if (!conf) return false;
        let id = $(this).data('id');
        $.ajax({
            url: `${target}/delete`,
            method: 'POST',
            dataType: 'json',
            data: {
                id
            },
            success: data => {
                alert(data.message);
                if (data.status == 200) {
                    location.reload();
                }
            },
            error: (e) => {
                alert(`${e.status} - ${e.statusText}`);
            }
        })
    })

    initiateDatatables();
})