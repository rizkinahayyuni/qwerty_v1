$(document).ready(function () {
    const form = document.getElementById('add_master_pekerjaan')

    var validator = FormValidation.formValidation(form, {
        fields: {
            nama_master_pekerjaan: {
                validators: {
                    notEmpty: {
                        message: 'Mohon isi nama master pekerjaan'
                    }
                }
            },
        },

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    })

    $('#add_master_pekerjaan').submit(function (e) {
        e.preventDefault()
        if (validator) {
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    $('#kt_sign_in_submit').attr('data-kt-indicator', 'on')
                    $('#kt_sign_in_submit').attr('disabled', true)

                    $.ajax({
                        url: `${target}/add_process`,
                        type: 'POST',
                        data: $('#add_master_pekerjaan').serialize(),
                        dataType: 'json',
                        success: data => {
                            alert(data.message)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                            if (data.status == 200) {
                                window.location.replace(`${target}`)
                            }
                        },
                        error: e => {
                            alert(`${e.status} - ${e.statusText}`)
                            $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                            $('#kt_sign_in_submit').removeAttr('disabled')
                        }
                    })
                }
            })
        }
    })
})
