$(document).ready(function () {
  // start stepper
  var element = document.querySelector('#kt_stepper_example_basic')

  // Initialize Stepper
  var stepper = new KTStepper(element)

  // Handle next step
  stepper.on('kt.stepper.next', function (stepper) {
    if (stepper.currentStepIndex == 1) {
      if (validator_about_me) {
        validator_about_me.validate().then(function (status) {
          if (status == 'Valid') {
            stepper.goNext()
          }
        })
      }
    } else if (stepper.currentStepIndex == 2) {
      if (validator_pendidikan) {
        validator_pendidikan.validate().then(function (status) {
          if (status == 'Valid') {
            stepper.goNext()
          }
        })
      }
    } else if (stepper.currentStepIndex == 3) {
      if (validator_pekerjaan) {
        validator_pekerjaan.validate().then(function (status) {
          if (status == 'Valid') {
            stepper.goNext()
          }
        })
      }
    } else {
      stepper.goNext()
    }
  })

  // Handle previous step
  stepper.on('kt.stepper.previous', function (stepper) {
    stepper.goPrevious() // go previous step
  })
  // end stepper

  // start step 1 : about me
  const form_about_me = document.getElementById('form_add_cv_about_me')

  var validator_about_me = FormValidation.formValidation(form_about_me, {
    fields: {
      about_us: {
        validators: {
          notEmpty: {
            message: 'Mohon isi about me'
          }
        }
      }
    },
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: '.fv-row',
        eleInvalidClass: '',
        eleValidClass: ''
      })
    }
  })
  // end step 1

  // start step 2 : pendidikan

  // start validation
  const form_pendidikan = document.getElementById('form_add_cv_pendidikan')

  var validator_pendidikan = FormValidation.formValidation(form_pendidikan, {
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: '.fv-row',
        eleInvalidClass: '',
        eleValidClass: ''
      })
    }
  })

  const addFields_pendidikan = function (index) {
    const namePrefix = 'form_add_cv_pendidikan_repeater[' + index + ']'

    validator_pendidikan.addField(namePrefix + '[id_tingkat_pendidikan]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih tingkat pendidikan'
        }
      }
    })

    validator_pendidikan.addField(namePrefix + '[id_nama_sekolah]', {
      validators: {
        notEmpty: {
          message: 'Mohon isi nama sekolah/perguruan tinggi'
        }
      }
    })

    validator_pendidikan.addField(namePrefix + '[tahun_masuk]', {
      validators: {
        notEmpty: {
          message: 'Mohon isi tahun masuk'
        }
      }
    })

    validator_pendidikan.addField(namePrefix + '[tahun_keluar]', {
      validators: {
        notEmpty: {
          message: 'Mohon isi tahun keluar'
        }
      }
    })
  }

  const removeFields_pendidikan = function (index) {
    const namePrefix = 'form_add_cv_pendidikan_repeater[' + index + ']'

    validator_pendidikan.removeField(namePrefix + '[id_tingkat_pendidikan]')
    validator_pendidikan.removeField(namePrefix + '[id_nama_sekolah]')
    validator_pendidikan.removeField(namePrefix + '[tahun_masuk]')
    validator_pendidikan.removeField(namePrefix + '[tahun_keluar]')
  }
  // end validation

  // start year
  const setTahunPendidikan = function () {
    $('.pendidikan_tahun').each(function (index, element) {
      new tempusDominus.TempusDominus(element, {
        restrictions: {
          maxDate: new Date()
        },
        display: {
          // viewMode: 'clock',
          components: {
            decades: true,
            year: true,
            month: false,
            date: false,
            hours: false,
            minutes: false,
            seconds: false
          }
        }
      })
    })
  }
  // end year

  $('#form_add_cv_pendidikan').on(
    'change',
    '.form_select_tingkat_pendidikan',
    function (e) {
      e.preventDefault()
      let id_tingkat_pendidikan = $(this).val()

      let tingkat_perguruan_tinggi = ['56', '57', '58']

      if (tingkat_perguruan_tinggi.includes(id_tingkat_pendidikan)) {
        $(this)
          .closest('[data-repeater-item]')
          .find('.form_select_jurusan')
          .removeAttr('disabled')
      } else {
        $(this)
          .closest('[data-repeater-item]')
          .find('.form_select_jurusan')
          .val('')
          .trigger('change')

        $(this)
          .closest('[data-repeater-item]')
          .find('.form_select_jurusan')
          .attr('disabled', true)
      }
    }
  )

  // start repeater
  $('#form_add_cv_pendidikan_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      addFields_pendidikan(index)
      setTahunPendidikan()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_tingkat_pendidikan')
        .select2({
          placeholder: '-- Pilih tingkat pendidikan --'
        })

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_nama_sekolah')
        .select2({
          placeholder: '-- Pilih tingkat nama sekolah --'
        })

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_jurusan')
        .select2({
          placeholder: '-- Pilih tingkat jurusan --'
        })
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_pendidikan(index)
    }
  })
  // end repeater

  // start select2
  $(
    '[name="form_add_cv_pendidikan_repeater[0][id_tingkat_pendidikan]"]'
  ).select2({
    placeholder: '-- Pilih tingkat pendidikan --'
  })

  $('[name="form_add_cv_pendidikan_repeater[0][id_nama_sekolah]"]').select2({
    placeholder: '-- Pilih tingkat nama sekolah --'
  })

  $('[name="form_add_cv_pendidikan_repeater[0][id_jurusan]"]').select2({
    placeholder: '-- Pilih tingkat jurusan --'
  })
  // end select2

  setTahunPendidikan()

  addFields_pendidikan(0)

  // end step 2

  // start step 3 : pekerjaan

  // start validation
  const form_pekerjaan = document.getElementById('form_add_cv_pekerjaan')

  var validator_pekerjaan = FormValidation.formValidation(form_pekerjaan, {
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: '.fv-row',
        eleInvalidClass: '',
        eleValidClass: ''
      })
    }
  })

  const addFields_pekerjaan = function (index) {
    const namePrefix = 'form_add_cv_pekerjaan_repeater[' + index + ']'

    validator_pekerjaan.addField(namePrefix + '[id_perusahaan]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih nama perusahaan'
        }
      }
    })

    validator_pekerjaan.addField(namePrefix + '[tanggal_masuk]', {
      validators: {
        notEmpty: {
          message: 'Mohon isi tanggal masuk'
        }
      }
    })

    validator_pekerjaan.addField(namePrefix + '[tanggal_keluar]', {
      validators: {
        notEmpty: {
          message: 'Mohon isi tanggal keluar'
        }
      }
    })
  }

  const removeFields_pekerjaan = function (index) {
    const namePrefix = 'form_add_cv_pekerjaan_repeater[' + index + ']'

    validator_pekerjaan.removeField(namePrefix + '[id_perusahaan]')
    validator_pekerjaan.removeField(namePrefix + '[tanggal_masuk]')
    validator_pekerjaan.removeField(namePrefix + '[tanggal_keluar]')
  }
  // end validation

  // start year
  const setTanggalKerja = function () {
    $('.pekerjaan_tanggal').each(function (index, element) {
      picker = new tempusDominus.TempusDominus(element, {
        restrictions: {
          maxDate: new Date()
        },
        display: {
          components: {
            decades: true,
            year: true,
            month: true,
            date: true,
            hours: false,
            minutes: false,
            seconds: false
          }
        },
        localization: {
          format: 'yyyy-MM-dd'
        }
      }).dates.formatInput = date => moment(date).format('YYYY-MM-DD')

      // picker.
    })
  }
  // end year

  // start repeater
  $('#form_add_cv_pekerjaan_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    repeaters: [
      {
        selector: '.inner-repeater',
        show: function () {
          $(this).slideDown()
        },

        hide: function (deleteElement) {
          $(this).slideUp(deleteElement)
        }
      }
    ],

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_perusahaan')
        .select2({
          placeholder: '-- Pilih perusahaan --'
        })

      $(this)
        .closest('[data-repeater-item]')
        .find('.pekerjaan_tanggal')
        .each(function (k0, element) {
          new tempusDominus.TempusDominus(element, {
            restrictions: {
              maxDate: new Date()
            },
            display: {
              components: {
                decades: true,
                year: true,
                month: true,
                date: true,
                hours: false,
                minutes: false,
                seconds: false
              }
            },
            localization: {
              format: 'yyyy-MM-dd'
            }
          }).dates.formatInput = date => moment(date).format('YYYY-MM-DD')
        })

      addFields_pekerjaan(index)
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_pekerjaan(index)
    }
  })

  addFields_pekerjaan(0)
  // end repeater

  // start select2
  $('[name="form_add_cv_pekerjaan_repeater[0][id_perusahaan]"]').select2({
    placeholder: '-- Pilih perusahaan --'
  })

  setTanggalKerja()

  // end step 3

  // start step 4 : skills

  // start validation

  const form_skills = document.getElementById('form_add_cv_skills')

  var validator_skills = FormValidation.formValidation(form_skills, {
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: '.fv-row',
        eleInvalidClass: '',
        eleValidClass: ''
      })
    }
  })

  // start skills software
  const addFields_skills_software = function (index) {
    const namePrefix = 'form_add_cv_skills_software_repeater[' + index + ']'

    validator_skills.addField(namePrefix + '[id_software]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih software'
        }
      }
    })
  }

  const removeFields_skills_software = function (index) {
    const namePrefix = 'form_add_cv_skills_software_repeater[' + index + ']'

    validator_skills.removeField(namePrefix + '[id_software]')
  }

  $('#form_add_cv_skills_software_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_software')
        .select2({
          placeholder: '-- Pilih software --'
        })

      addFields_skills_software(index)

      console.log(
        $(this).closest('[data-repeater-item]').find('.rating-label').length
      )

      $(this)
        .closest('[data-repeater-item]')
        .find('.rating-label')
        .each(function (k0, v0) {
          let for_val = $(this).attr('for').split('_')

          let for_val_new_arr = [
            for_val[0],
            for_val[1],
            for_val[2],
            index,
            for_val[4]
          ]
          let for_val_new_str = for_val_new_arr.join('_')

          $(this).attr('for', for_val_new_str)
        })

      $(this)
        .closest('[data-repeater-item]')
        .find('.rating-input')
        .each(function (k0, v0) {
          let id_val = $(this).attr('id').split('_')

          let id_val_new_arr = [
            id_val[0],
            id_val[1],
            id_val[2],
            index,
            id_val[4]
          ]
          let id_val_new_str = id_val_new_arr.join('_')

          $(this).attr('id', id_val_new_str)
        })
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_skills_software(index)
    }
  })

  addFields_skills_software(0)
  $('[name="form_add_cv_skills_software_repeater[0][id_software]"]').select2({
    placeholder: '-- Pilih software --'
  })
  // end skills software

  // start skills bahasa
  const addFields_skills_bahasa = function (index) {
    const namePrefix = 'form_add_cv_skills_bahasa_repeater[' + index + ']'

    validator_skills.addField(namePrefix + '[id_bahasa]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih bahasa'
        }
      }
    })
  }

  const removeFields_skills_bahasa = function (index) {
    const namePrefix = 'form_add_cv_skills_bahasa_repeater[' + index + ']'

    validator_skills.removeField(namePrefix + '[id_bahasa]')
  }

  $('#form_add_cv_skills_bahasa_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_bahasa')
        .select2({
          placeholder: '-- Pilih bahasa --'
        })

      addFields_skills_bahasa(index)

      console.log(
        $(this).closest('[data-repeater-item]').find('.rating-label').length
      )

      $(this)
        .closest('[data-repeater-item]')
        .find('.rating-label')
        .each(function (k0, v0) {
          let for_val = $(this).attr('for').split('_')

          let for_val_new_arr = [
            for_val[0],
            for_val[1],
            for_val[2],
            index,
            for_val[4]
          ]
          let for_val_new_str = for_val_new_arr.join('_')

          $(this).attr('for', for_val_new_str)
        })

      $(this)
        .closest('[data-repeater-item]')
        .find('.rating-input')
        .each(function (k0, v0) {
          let id_val = $(this).attr('id').split('_')

          let id_val_new_arr = [
            id_val[0],
            id_val[1],
            id_val[2],
            index,
            id_val[4]
          ]
          let id_val_new_str = id_val_new_arr.join('_')

          $(this).attr('id', id_val_new_str)
        })
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_skills_bahasa(index)
    }
  })

  addFields_skills_bahasa(0)
  $('[name="form_add_cv_skills_bahasa_repeater[0][id_bahasa]"]').select2({
    placeholder: '-- Pilih bahasa --'
  })
  // end skills bahasa

  // start skills skill
  const addFields_skills_skill = function (index) {
    const namePrefix = 'form_add_cv_skills_skill_repeater[' + index + ']'

    validator_skills.addField(namePrefix + '[id_skill]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih keahlian'
        }
      }
    })
  }

  const removeFields_skills_skill = function (index) {
    const namePrefix = 'form_add_cv_skills_skill_repeater[' + index + ']'

    validator_skills.removeField(namePrefix + '[id_skill]')
  }

  $('#form_add_cv_skills_skill_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_skill')
        .select2({
          placeholder: '-- Pilih keahlian --'
        })

      addFields_skills_skill(index)
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_skills_skill(index)
    }
  })

  addFields_skills_skill(0)
  $('[name="form_add_cv_skills_skill_repeater[0][id_skill]"]').select2({
    placeholder: '-- Pilih keahlian --'
  })
  // end skills skill

  // start skills sertifikasi
  const addFields_skills_sertifikasi = function (index) {
    const namePrefix = 'form_add_cv_skills_sertifikasi_repeater[' + index + ']'

    validator_skills.addField(namePrefix + '[id_sertifikasi]', {
      validators: {
        notEmpty: {
          message: 'Mohon pilih keahlian'
        }
      }
    })
  }

  const removeFields_skills_sertifikasi = function (index) {
    const namePrefix = 'form_add_cv_skills_sertifikasi_repeater[' + index + ']'

    validator_skills.removeField(namePrefix + '[id_sertifikasi]')
  }

  $('#form_add_cv_skills_sertifikasi_repeater').repeater({
    initEmpty: false,

    defaultValues: {
      'text-input': 'foo'
    },

    show: function () {
      $(this).slideDown()

      const index = $(this).closest('[data-repeater-item]').index()

      $(this)
        .closest('[data-repeater-item]')
        .find('.form_select_sertifikasi')
        .select2({
          placeholder: '-- Pilih sertifikasi --'
        })

      addFields_skills_sertifikasi(index)
    },

    hide: function (deleteElement) {
      $(this).slideUp(deleteElement)

      const index = $(this).closest('[data-repeater-item]').index()

      removeFields_skills_sertifikasi(index)
    }
  })

  addFields_skills_sertifikasi(0)
  $(
    '[name="form_add_cv_skills_sertifikasi_repeater[0][id_sertifikasi]"]'
  ).select2({
    placeholder: '-- Pilih sertifikasi --'
  })
  // end skills sertifikasi

  // end step 4

  $('#stepper-submit').click(function (e) {
    e.preventDefault()

    $('#form_add_cv_skills').trigger('submit')
  })

  $('#form_add_cv_skills').submit(function (e) {
    e.preventDefault()

    if (validator_skills) {
      validator_skills.validate().then(function (status) {
        if (status == 'Valid') {
          $('#stepper-submit').attr('data-kt-indicator', 'on')
          $('#stepper-submit').attr('disabled', true)
        }
      })

      let form_about_me = $('#form_add_cv_about_me').serialize()
      let form_pendidikan = $('#form_add_cv_pendidikan').serialize()
      let form_pekerjaan = $('#form_add_cv_pekerjaan').serialize()
      let form_skills = $('#form_add_cv_skills').serialize()

      let form_cv =
        form_about_me +
        '&' +
        form_pendidikan +
        '&' +
        form_pekerjaan +
        '&' +
        form_skills

      $.ajax({
        url: `${target}/add_cv_process`,
        type: 'POST',
        data: form_cv,
        dataType: 'json',
        success: data => {
          if (data.status == 200) {
            Swal.fire('Berhasil !', data.message, 'success').then(results => {
              $('#stepper-submit').removeAttr('data-kt-indicator')
              $('#stepper-submit').removeAttr('disabled')

              window.location.replace(`${target}`)
            })
          } else {
            Swal.fire('Gagal !', data.message, 'error').then(results => {
              $('#stepper-submit').removeAttr('data-kt-indicator')
              $('#stepper-submit').removeAttr('disabled')
            })
          }
        },
        error: e => {
          Swal.fire('Gagal !', `${e.status} - ${e.statusText}`, 'error').then(
            results => {
              $('#stepper-submit').removeAttr('data-kt-indicator')
              $('#stepper-submit').removeAttr('disabled')
            }
          )
        }
      })
    }
  })
})
