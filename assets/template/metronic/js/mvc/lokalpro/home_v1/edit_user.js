$(document).ready(function () {
  new tempusDominus.TempusDominus(document.getElementById('lahir_tanggal'), {
    restrictions: {
      maxDate: new Date()
    },
    display: {
      // viewMode: 'clock',
      components: {
        decades: true,
        year: true,
        month: true,
        date: true,
        hours: false,
        minutes: false,
        seconds: false
      }
    }
  })

  $('#alamat3').select2({
    placeholder: 'Pilih alamat',
    allowClear: true,
    minimumInputLength: 3,
    ajax: {
      dataType: 'json',
      url: `${target}/get_s2_alamat`,
      delay: 600,
      data: function (params) {
        return {
          search: params.term
        }
      },
      processResults: function (data, page) {
        return {
          results: data
        }
      }
    }
  })

  $('#alamat3, #alamat1, #rt, #rw, #kode_pos').change(function (e) {
    view_alamat()
  })

  view_alamat()

  const form = document.getElementById('edit_user_form')

  var validator = FormValidation.formValidation(form, {
    fields: {
      nama_lengkap: {
        validators: {
          notEmpty: {
            message: 'Mohon isi nama lengkap'
          }
        }
      },

      no_ktp: {
        validators: {
          notEmpty: {
            message: 'Mohon isi No. KTP'
          },
          stringLength: {
            max: 16,
            min: 16,
            message: 'No. KTP harus 16 digit'
          }
        }
      },

      gender: {
        validators: {
          notEmpty: {
            message: 'Mohon pilih jenis kelamin'
          }
        }
      },

      lahir_tanggal: {
        validators: {
          notEmpty: {
            message: 'Mohon isi tanggal lahir'
          }
        }
      },

      email: {
        validators: {
          notEmpty: {
            message: 'Mohon isi email'
          },
          emailAddress: {
            message: 'Format email tidak sesuai',
            requireGlobalDomain: true
          }
        }
      }
    },

    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      bootstrap: new FormValidation.plugins.Bootstrap5({
        rowSelector: '.fv-row',
        eleInvalidClass: '',
        eleValidClass: ''
      })
    }
  })

  $('#edit_user_form').submit(function (e) {
    e.preventDefault()

    if (validator) {
      validator.validate().then(function (status) {
        if (status == 'Valid') {
          $('#kt_sign_in_submit').attr('data-kt-indicator', 'on')
          $('#kt_sign_in_submit').attr('disabled', true)

          $.ajax({
            url: `${target}/edit_user_process`,
            type: 'POST',
            data: $('#edit_user_form').serialize(),
            dataType: 'json',
            success: data => {
              if (data.status == 200) {
                Swal.fire('Berhasil !', data.message, 'success').then(
                  results => {
                    $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                    $('#kt_sign_in_submit').removeAttr('disabled')

                    window.location.replace(`${target}`)
                  }
                )
              } else {
                Swal.fire('Gagal !', data.message, 'error').then(results => {
                  $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                  $('#kt_sign_in_submit').removeAttr('disabled')
                })
              }
            },
            error: e => {
              Swal.fire(
                'Gagal !',
                `${e.status} - ${e.statusText}`,
                'error'
              ).then(results => {
                $('#kt_sign_in_submit').removeAttr('data-kt-indicator')
                $('#kt_sign_in_submit').removeAttr('disabled')
              })
            }
          })
        }
      })
    }
  })

  function view_alamat () {
    $('#view_alamat').html('')

    let alamat_1 = $('#alamat1').val()
    let alamat_3 = $('#alamat3').val()
    let rt = $('#rt').val()
    let rw = $('#rw').val()
    let kode_pos = $('#kode_pos').val()

    let row_1 = []
    let row_2 = []
    let row_3 = []

    if (alamat_1 !== '') {
      row_1.push(alamat_1)
    }

    if (rt !== '' && rw !== '') {
      row_1.push('RT ' + rt + '/' + rw)
    }

    if (alamat_3 !== '') {
      row_2.push(alamat_3)
    }

    if (kode_pos !== '') {
      row_3.push('Kode Pos : ' + kode_pos)
    }

    let alamat_array = [row_1.join(', '), row_2.join(', '), row_3.join(', ')]
    let alamat_array_filtred = alamat_array.filter(function (val) {
      return val !== ''
    })

    let alamat_str = alamat_array_filtred.join('<br/>')

    $('#view_alamat').html(alamat_str)
  }
})
