$(document).ready(function () {
  // Define form element
  const form = document.getElementById('kt_sign_in_form');


  var validator = FormValidation.formValidation(
    form,
    {
        fields: {
            'email': {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                      message: 'Email format is invalid',
                      requireGlobalDomain: true
                    }
                }
            },
            
            'password': {
              validators: {
                  notEmpty: {
                      message: 'Password is required'
                  }
              }
          },
        },

        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap: new FormValidation.plugins.Bootstrap5({
                rowSelector: '.fv-row',
                eleInvalidClass: '',
                eleValidClass: ''
            })
        }
    }
  );

  $('#kt_sign_in_form').submit(function (e) { 
    e.preventDefault();
    

    // Validate form before submit
    if (validator) {
      validator.validate().then(function (status) {
          console.log('validated!');

          if (status == 'Valid') {
              $('#kt_sign_in_submit').attr('data-kt-indicator', 'on');
              $('#kt_sign_in_submit').attr('disabled', true);

              setTimeout(function () {

                $.ajax({
                  url: `${target}/login_process`,
                  type: "POST",
                  data: $('#kt_sign_in_form').serialize(),
                  dataType: 'json',
                  success: (data) => {
                    

                    if (data.status == 200) {
                      $('#kt_sign_in_submit').removeAttr('data-kt-indicator');
                      $('#kt_sign_in_submit').removeAttr('disabled');

                      window.location.replace(`${data.redirect}`);
                    } else {
                      $('#kt_sign_in_submit').removeAttr('data-kt-indicator');
                      $('#kt_sign_in_submit').removeAttr('disabled');
                      
                      Swal.fire({
                        title: "Error!",
                        text: data.message,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok",
                        customClass: {
                          confirmButton: "btn btn-danger"
                        }
                      });
                    }

                  },
                  error: (e) => {
                    


                    Swal.fire({
                      title: "Error!",
                      text: `${e.status} - ${e.statusText}`,
                      icon: "error",
                      buttonsStyling: false,
                      confirmButtonText: "Ok",
                      customClass: {
                        confirmButton: "btn btn-danger"
                      }
                    });

                    $('#kt_sign_in_submit').removeAttr('data-kt-indicator');
                    $('#kt_sign_in_submit').removeAttr('disabled');

                  }
                });                


                
              }, 500);
          }
      });
    }
  });
});