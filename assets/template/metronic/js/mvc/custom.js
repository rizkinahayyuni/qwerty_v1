function padLeadingZeros (num, size) {
  var s = num + ''
  while (s.length < size) s = '0' + s
  return s
}

function number_only (evt) {
  var theEvent = evt || window.event

  // Handle paste
  if (theEvent.type === 'paste') {
    key = event.clipboardData.getData('text/plain')
  } else {
    // Handle key press
    var key = theEvent.keyCode || theEvent.which
    key = String.fromCharCode(key)
  }
  var regex = /[0-9]|\./
  if (!regex.test(key)) {
    theEvent.returnValue = false
    if (theEvent.preventDefault) theEvent.preventDefault()
  }
}

function showOverlay (
  text = 'Sedang dalam proses... Mohon tunggu sebentar ...'
) {
  $.LoadingOverlay('show', {
    image: '',
    text: text,
    textClass: 'fs-1 fw-bold text-center text-gray-600'
  })
}

function hideOverlay () {
  $.LoadingOverlay('hide')
}
