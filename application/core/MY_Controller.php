<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');

/**
 * MY controller
 *
 * Seluruh controller bisa extends ke MY_Controller.
 * Opsi ini akan menghemat pengkodingan terkait dengan response untuk authentication
 *
 * @extends CI_Controller
 */

class MY_Controller extends CI_Controller
{
  /**
   * Data token
   *
   * @var array
   * @access protected
   */
  // protected $data_token = [];

  /**
   * Constructor
   *
   * @access public
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
    ini_set('xdebug.var_display_max_children', -1);
    ini_set('xdebug.var_display_max_depth', -1);
    ini_set('xdebug.var_display_max_data', -1);

    $timezone = 'Asia/Jakarta';
    date_default_timezone_set($timezone);

    $this->uri_app = explode('/', str_replace(base_url(), '', current_url()));
    $this->class = str_replace(substr($this->router->fetch_class(), 0, 3), substr($this->router->fetch_class(), 2, 1), $this->router->fetch_class());
    $this->_assets = base_url('assets/template/metronic/');

    $this->navbar = [
      'nb3_aktif' => [
        'id_navbar_level3' => '1-1-1',
        'id_navbar_level2' => '1-1',
        'id_navbar_level1' => '1',
        'nama' => 'Pendidikan'
      ],
      'nb2_aktif' => [
        'id_navbar_level2' => '1-1',
        'id_navbar_level1' => '1',
        'nama' => 'Pendidikan'
      ],
      'nb2_all' => [
        [
          'id_navbar_level2' => '1-1',
          'id_navbar_level1' => '1',
          'nama' => 'Pendidikan',
          'nb_1' => [
            [
              'id_navbar_level3' => '1-1-1',
              'id_navbar_level2' => '1-1',
              'route' => 'master_pendidikan',
              'nama' => 'Pendidikan',
            ]
          ]
        ],
        [
          'id_navbar_level2' => '1-2',
          'id_navbar_level1' => '1',
          'nama' => 'Skill',
          'nb_1' => [
            [
              'id_navbar_level3' => '1-2-1',
              'id_navbar_level2' => '1-2',
              'route' => 'master_skill',
              'nama' => 'Skill',
            ]
          ]
        ],
        [
          'id_navbar_level2' => '1-3',
          'id_navbar_level1' => '1',
          'nama' => 'Software',
          'nb_1' => [
            [
              'id_navbar_level3' => '1-3-1',
              'id_navbar_level2' => '1-3',
              'route' => 'master_software',
              'nama' => 'Software',
            ]
          ]
        ],
        [
          'id_navbar_level2' => '1-4',
          'id_navbar_level1' => '1',
          'nama' => 'Pekerjaan',
          'nb_1' => [
            [
              'id_navbar_level3' => '1-4-1',
              'id_navbar_level2' => '1-4',
              'route' => 'master_pekerjaan',
              'nama' => 'Pekerjaan',
            ]
          ]
        ],
      ]
    ];
  }
}
