<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['api/master_skill'] = 'qwerty_v1/api/master_v1/C_master_skill_v1';
$route['api/master_skill/(:any)'] = 'qwerty_v1/api/master_v1/C_master_skill_v1/$1';
$route['api/master_pendidikan'] = 'qwerty_v1/api/master_v1/C_master_pendidikan_v1';
$route['api/master_pendidikan/(:any)'] = 'qwerty_v1/api/master_v1/C_master_pendidikan_v1/$1';
$route['api/master_software'] = 'qwerty_v1/api/master_v1/C_master_software_v1';
$route['api/master_software/(:any)'] = 'qwerty_v1/api/master_v1/C_master_software_v1/$1';
$route['api/master_pekerjaan'] = 'qwerty_v1/api/master_v1/C_master_pekerjaan_v1';
$route['api/master_pekerjaan/(:any)'] = 'qwerty_v1/api/master_v1/C_master_pekerjaan_v1/$1';