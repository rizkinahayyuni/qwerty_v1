<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['master_skill'] = 'qwerty_v1/mvc/master_v1/C_master_skill_v1';
$route['master_skill/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_skill_v1/$1';
$route['master_pendidikan'] = 'qwerty_v1/mvc/master_v1/C_master_pendidikan_v1';
$route['master_pendidikan/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_pendidikan_v1/$1';
$route['master_software'] = 'qwerty_v1/mvc/master_v1/C_master_software_v1';
$route['master_software/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_software_v1/$1';
$route['master_pekerjaan'] = 'qwerty_v1/mvc/master_v1/C_master_pekerjaan_v1';
$route['master_pekerjaan/(:any)'] = 'qwerty_v1/mvc/master_v1/C_master_pekerjaan_v1/$1';