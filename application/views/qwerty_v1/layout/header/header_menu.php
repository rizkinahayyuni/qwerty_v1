<div class="menu menu-rounded menu-column menu-lg-row my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
  id="kt_app_header_menu" data-kt-menu="true">
  <?php foreach ($this->navbar['nb2_all'] as $k0 => $v0): ?>
    <?php if (count($v0['nb_1']) == 1): ?>
      <div data-kt-menu-placement="bottom-start"
        class="menu-item menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
        <a href="<?= base_url($v0['nb_1'][0]['route']) ?>"
          class="menu-link <?= $v0['nb_1'][0]['id_navbar_level3'] == $this->navbar['nb3_aktif']['id_navbar_level3'] ? 'active' : ''; ?>">
          <span class="menu-title">
            <?= $v0['nb_1'][0]['nama']; ?>
          </span>
          <span class="menu-arrow d-lg-none"></span>
        </a>
      </div>
    <?php else: ?>
      <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start"
        class="menu-item menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
        <span class="menu-link">
          <span class="menu-title">
            <?= $v0['nama'] ?>
          </span>
          <span class="menu-arrow d-lg-none"></span>
        </span>
        <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-250px">
          <?php foreach ($v0['nb_1'] as $k1 => $v1): ?>
            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start"
              class="menu-item menu-lg-down-accordion">
              <div class="menu-item">
                <a class="menu-link <?= $v1['id_navbar_level3'] == $this->navbar['nb3_aktif']['id_navbar_level3'] ? 'active' : ''; ?>"
                  href="<?= base_url($v1['route']) ?>">
                  <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                  </span>
                  <span class="menu-title">
                    <?= $v1['nama']; ?>
                  </span>
                </a>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
  <?php endforeach; ?>
</div>