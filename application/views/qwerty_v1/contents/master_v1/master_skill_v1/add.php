<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                    <?= $title ?>
                </h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
            <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form w-100" novalidate="novalidate" id="add_master_skill" action="#">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <select class="form-select" aria-label="Default select example"
                                                    id="id_ref_master_tipe" name="id_ref_master_tipe">
                                                    <option value="57" selected>SKILL</option>
                                                </select>
                                                <label for="nama_master_pendidikan">Nama Master <span
                                                        class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <select class="form-select" aria-label="Default select example"
                                                    id="tipe_skills" name="tipe_skills">
                                                    <option value="">-</option>
                                                    <option value="IT">IT</option>
                                                    <option value="KEUANGAN">KEUANGAN</option>
                                                </select>
                                                <label for="nama_master_pendidikan">Kategori Skill <span
                                                        class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="fv-row mb-8">
                                            <div class="form-floating">
                                                <input type="text" class="form-control" id="nama_master_skill"
                                                    name="nama_master_skill" placeholder="" value="" />
                                                <label for="nama_master_skill">Nama Master Skill <span
                                                        class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <a href="<?= base_url($this->_class) ?>" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                                        <span class="indicator-label">Add</span>

                                        <span class="indicator-progress">Please wait...
                                            <span
                                                class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>
<!--end::Content wrapper-->