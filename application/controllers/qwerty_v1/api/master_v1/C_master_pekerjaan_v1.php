<?php defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . "libraries/format.php";
require APPPATH . "libraries/RestController.php";

use chriskacerguis\RestServer\RestController;

class C_master_pekerjaan_v1 extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('qwerty_v1/master_v1/M_master_pekerjaan_v1', 'master_pekerjaan');
        $this->id_ref_master_tipe = 91;
    }


    public function index_get()
    {
        $get = $this->get();
        $res = $this->master_pekerjaan->get($get);
        $this->response($res, $res['status']);
    }

    public function index_post()
    {
        $post = $this->post();
        $res = $this->master_pekerjaan->add($post);
        $this->response($res, $res['status']);
    }

    public function index_put()
    {
        $put = $this->put();
        $res = $this->master_pekerjaan->update($put);
        $this->response($res, $res['status']);
    }

    public function index_delete()
    {
        $delete = $this->delete();
        $res = $this->master_pekerjaan->delete($delete);
        $this->response($res, $res['status']);
    }
}

/* End of file C_master_pekerjaan_v1.php */
