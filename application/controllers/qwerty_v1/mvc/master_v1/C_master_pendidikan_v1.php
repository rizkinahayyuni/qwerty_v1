<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_master_pendidikan_v1 extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_class = $this->uri->segments[1];
        $this->_view = 'qwerty_v1/contents/master_v1/master_pendidikan_v1';
        $this->load->model('qwerty_v1/master_v1/M_master_pendidikan_v1', 'master_pendidikan');

        $this->id_ref_master_tipe = 82;

        $this->navbar['nb3_aktif'] = [
            'id_navbar_level3' => '1-1-1',
            'id_navbar_level2' => '1-1',
            'id_navbar_level1' => '1',
            'nama' => 'Pendidikan'
        ];
        $this->navbar['nb2_aktif'] = [
            'id_navbar_level2' => '1-1',
            'id_navbar_level1' => '1',
            'nama' => 'Pendidikan'
        ];
    }


    public function index()
    {
        $data['title'] = "Master Pendidikan";
        $data['url'] = base_url($this->_class);
        $data['contents'] = $this->_view . '/index';
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_pendidikan_v1/index.js',
        ];
        $data['css'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_pendidikan_v1/style.css',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    function json()
    {
        $value = $this->_postvalue()['data'];
        $json = [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $value['record_total'],
            'recordsFiltered' => $value['record_filter']
        ];

        $data = [];

        $i = $_POST['start'];
        foreach ($value['result'] as $k => $v) {
            $i++;
            $row = [];
            $button = '<a href="' . base_url($this->_class . '/edit?id=' . $v['id']) . '" class="btn btn-sm btn-primary">Edit</a>';
            $button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id'] . '" style="margin-left:5px">Hapus</button>';
            $row[] = $i;
            $row[] = $button;
            $row[] = $v['desc_global_master'];

            $data[] = $row;
        }

        $json['data'] = $data;

        echo json_encode($json);
    }

    function _postvalue($paging = TRUE)
    {
        $params = [
            'offset' => $this->input->post('start'),
            'limit' => $this->input->post('length'),
            'search' => $this->input->post('search')['value'],
        ];


        if ($_POST['search']['value']) {
            $params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
        }

        if (isset($_POST['ORDER'])) {
            if ($_POST['order']['0']['column']) {
                $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
            }
            if ($_POST['order']['0']['dir']) {
                $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
            }
        }

        if (!$paging) {
            unset($params['offset']);
            unset($params['limit']);
        }

        $value = $this->master_pendidikan->get($params);
        return $value;
    }

    public function add($params = null)
    {
        $data['title'] = "Add Master Pendidikan";
        $data['contents'] = $this->_view . '/add';
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_pendidikan_v1/add.js',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    public function add_process($params = null)
    {
        $post = $this->input->post();
        $res_add = $this->master_pendidikan->add([
            'id_ref_master_tipe' => $this->id_ref_master_tipe,
            'desc_global_master' => $post['nama_master_pendidikan'],
        ]);
        echo json_encode($res_add);
    }

    public function edit($params = null)
    {
        $data['title'] = "Edit Master Pendidikan";
        $data['contents'] = $this->_view . '/edit';
        $data['detail'] = $this->master_pendidikan->get(['id' => $this->input->get('id')])['data']['result'][0];
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_pendidikan_v1/edit.js',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    public function edit_process($params = null)
    {
        $post = $this->input->post();
        $res_update = $this->master_pendidikan->update([
            'id' => $post['id'],
            'id_ref_master_tipe' => $this->id_ref_master_tipe,
            'desc_global_master' => $post['nama_master_pendidikan'],
        ]);
        echo json_encode($res_update);
    }

    public function delete($params = null)
    {
        $post = $this->input->post();
        $res_delete = $this->master_pendidikan->delete([
            'id' => $post['id'],
        ]);
        echo json_encode($res_delete);
    }
}

/* End of file C_master_pendidikan_v1.php */
