<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_master_skill_v1 extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_class = $this->uri->segments[1];
        $this->_view = 'qwerty_v1/contents/master_v1/master_skill_v1';
        $this->load->model('qwerty_v1/master_v1/M_master_skill_v1', 'master_skill');

        $this->id_ref_master_tipe = 57;

        $this->navbar['nb3_aktif'] = [
            'id_navbar_level3' => '1-2-1',
            'id_navbar_level2' => '1-2',
            'id_navbar_level1' => '1',
            'nama' => 'Skill'
        ];
        $this->navbar['nb2_aktif'] = [
            'id_navbar_level2' => '1-2',
            'id_navbar_level1' => '1',
            'nama' => 'Skill'
        ];
    }


    public function index()
    {
        $data['title'] = "Master Skill";
        $data['url'] = base_url($this->_class);
        $data['contents'] = $this->_view . '/index';
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_skill_v1/index.js',
        ];
        $data['css'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_skill_v1/style.css',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    function json()
    {
        $value = $this->_postvalue()['data'];
        $json = [
            'draw' => $this->input->post('draw'),
            'recordsTotal' => $value['record_total'],
            'recordsFiltered' => $value['record_filter']
        ];

        $data = [];

        $i = $_POST['start'];
        foreach ($value['result'] as $k => $v) {
            $i++;
            $row = [];
            $button = '<a href="' . base_url($this->_class . '/edit?id=' . $v['id']) . '" class="btn btn-sm btn-primary">Edit</a>';
            $button .= '<button type="button" class="btn btn-sm btn-danger delete" data-id="' . $v['id'] . '" style="margin-left:5px">Hapus</button>';
            $row[] = $i;
            $row[] = $button;
            $row[] = $v['tipe_skills'];
            $row[] = $v['desc_global_master'];

            $data[] = $row;
        }

        $json['data'] = $data;

        echo json_encode($json);
    }

    function _postvalue($paging = TRUE)
    {
        $params = [
            'offset' => $this->input->post('start'),
            'limit' => $this->input->post('length'),
            'search' => $this->input->post('search')['value'],
        ];


        if ($_POST['search']['value']) {
            $params['search'] = $_POST['search']['value'] ? $_POST['search']['value'] : '';
        }

        if (isset($_POST['ORDER'])) {
            if ($_POST['order']['0']['column']) {
                $params['order_column'] = $_POST['order']['0']['column'] ? $_POST['order']['0']['column'] : '2';
            }
            if ($_POST['order']['0']['dir']) {
                $params['order_dir'] = $_POST['order']['0']['dir'] ? $_POST['order']['0']['dir'] : 'asc';
            }
        }

        if (!$paging) {
            unset($params['offset']);
            unset($params['limit']);
        }

        $value = $this->master_skill->get($params);
        return $value;
    }

    public function add($params = null)
    {
        $data['title'] = "Add Master Skill";
        $data['contents'] = $this->_view . '/add';
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_skill_v1/add.js',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    public function add_process($params = null)
    {
        $post = $this->input->post();
        $res_add = $this->master_skill->add([
            'id_ref_master_tipe' => $this->id_ref_master_tipe,
            'tipe_skills' => $post['tipe_skills'],
            'desc_global_master' => $post['nama_master_skill'],
        ]);
        echo json_encode($res_add);
    }

    public function edit($params = null)
    {
        $data['title'] = "Edit Master Skill";
        $data['contents'] = $this->_view . '/edit';
        $data['detail'] = $this->master_skill->get(['id' => $this->input->get('id')])['data']['result'][0];
        $data['js'] = [
            $this->_assets . 'qwerty_v1/js/master_v1/master_skill_v1/edit.js',
        ];

        $this->load->view('qwerty_v1/layout/master', $data);
    }

    public function edit_process($params = null)
    {
        $post = $this->input->post();
        $res_update = $this->master_skill->update([
            'id' => $post['id'],
            'id_ref_master_tipe' => $this->id_ref_master_tipe,
            'tipe_skills' => $post['tipe_skills'],
            'desc_global_master' => $post['nama_master_skill'],
        ]);
        echo json_encode($res_update);
    }

    public function delete($params = null)
    {
        $post = $this->input->post();
        $res_delete = $this->master_skill->delete([
            'id' => $post['id'],
        ]);
        echo json_encode($res_delete);
    }
}

/* End of file C_master_skill_v1.php */
